// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// const functions = require('firebase-functions');
const admin = require("firebase-admin");
// const algoliaSync = require('algolia-firestore-sync');

admin.initializeApp(functions.config().firebase);





const functions = require('firebase-functions');
const algoliasearch = require('algoliasearch');
const algoliaSync = require('algolia-firestore-sync');

// const algolia = algoliasearch('MDF6H13HEL', 'ef123605653ea0fb1321f7208c61d80a');
const algolia = algoliasearch('MDF6H13HEL', 'ef123605653ea0fb1321f7208c61d80a');
const index = algolia.initIndex('travel');

exports.syncIngredients = functions.firestore.document('/viajes/{id}').onWrite(
  event => {
    return algoliaSync.syncAlgoliaWithFirestore(index, event);
  }
);
