const ALGOLIA_APP_ID="MDF6H13HEL";
const ALGOLIA_API_KEY='ef123605653ea0fb1321f7208c61d80a';
const ALGOLIA_INDEX_NAME='travel'
const FIREBASE_DATABASE_URL="https://algolia-firebase-161ea.firebaseio.com"
const FIREBASE_PROJECT_ID="algolia-firebase-161ea";


const functions = require('firebase-functions');
const algoliasearch = require('algoliasearch');
const dotenv = require('dotenv');
const firebase = require('firebase');
const admin = require("firebase-admin");

admin.initializeApp(functions.config().firebase);

// load values from the .env file in this directory into process.env
dotenv.load();

// configure firebase
firebase.initializeApp({
  databaseURL: process.env.FIREBASE_DATABASE_URL,
});

const database = admin.firestore();

// configure algolia
const algolia = algoliasearch(
  process.env.ALGOLIA_APP_ID,
  process.env.ALGOLIA_API_KEY
);
const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);

// Get all contacts from Firebase
database.collection('/viajes').get(contacts => {
  // Build an array of all records to push to Algolia
  const records = [];
  contacts.forEach(contact => {
    // get the key and data from the snapshot
    const childKey = contact.key;
    const childData = contact.val();
    // We set the Algolia objectID as the Firebase .key
    childData.objectID = childKey;
    // Add object for indexing
    records.push(childData);
  });

  // Add or update new objects
  index
    .saveObjects(records)
    .then(() => {
      console.log('Contacts imported into Algolia');
    })
    .catch(error => {
      console.error('Error when importing contact into Algolia', error);
      process.exit(1);
    });
});
