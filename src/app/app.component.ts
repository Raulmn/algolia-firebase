import { Component } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data;
  id;



  private _db;

  constructor(private firestore: AngularFirestore) {
    this._db = firestore.firestore;
    this._db.collection('items').get().then((querySnapshot) => {

      querySnapshot.forEach((doc) => {
        this.data = doc.data();
        this.data['id'] = doc.id;
        // console.log(doc.id + ' => ' + doc.data());
      });

    });

  }

  search(search) {
    
  }


  // for(let i = 0; i < 20; i++) {
  //   let viajesData = {
  //     name: 'California'+Math.floor(Math.random() * (1 - 50)) + 50,
  //     lat: Math.floor(Math.random() * (14 - 45)) + 45,
  //     long: Math.floor(Math.random() * (14 - 45)) + 45,
  //     desc: 'viaje super chulo',
  //     type: 'Guide',
  //     nPersonas: Math.floor(Math.random() * (1 - 5)) + 5,
  //     nHabitaciones: Math.floor(Math.random() * (1 - 5)) + 5
  //   };
  //   this._db.collection('viajes').doc().set(viajesData).then((data) => console.log('viajes',data));
  // }


}
