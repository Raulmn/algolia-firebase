import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { NgAisModule } from 'angular-instantsearch';

import { AppComponent } from './app.component';

export const firebaseConfig = {
    apiKey: "AIzaSyC8sraT4YKTXp4NKiZVVrfKSm97DdB5dFs",
    authDomain: "algolia-firebase-161ea.firebaseapp.com",
    databaseURL: "https://algolia-firebase-161ea.firebaseio.com",
    projectId: "algolia-firebase-161ea",
    storageBucket: "algolia-firebase-161ea.appspot.com",
    messagingSenderId: "553128706709"
  };

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    NgAisModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
